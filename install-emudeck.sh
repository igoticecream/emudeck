#!/usr/bin/env bash

set -euo pipefail

SCRIPT_PATH=$(dirname -- "$( readlink -f -- "$0"; )";)

source "$SCRIPT_PATH/functions/helper.sh"
source "$SCRIPT_PATH/functions/helper_ra.sh"
source "$SCRIPT_PATH/functions/helper_srm.sh"
source "$SCRIPT_PATH/functions/helper_cemu.sh"

trap err_report ERR

# Export local bin to PATH
export PATH="$SCRIPT_PATH/assets/bin:$PATH"
# Make local bin executable
chmod +x "$SCRIPT_PATH/assets/bin/xmlstarlet"

echo "Setting up emulation folder on microsd"
MICROSD_PATH=$(get_microsd_path)
EMUDECK_PATH="$MICROSD_PATH/Emulation"
RETROACHIEVEMENTS_FILE="$SCRIPT_PATH/config/retroachievements.ini"

if [ ! -d "$MICROSD_PATH" ]; then
    echo "MicroSD not found"
    exit 1
else
    mkdir -p "$EMUDECK_PATH/roms"
    mkdir -p "$EMUDECK_PATH/bios"
    mkdir -p "$EMUDECK_PATH/cores"
    mkdir -p "$EMUDECK_PATH/config"
    mkdir -p "$EMUDECK_PATH/saves"
    mkdir -p "$EMUDECK_PATH/saves/retroarch/saves"
    mkdir -p "$EMUDECK_PATH/saves/retroarch/states"
    mkdir -p "$EMUDECK_PATH/saves/cemu"
    mkdir -p "$EMUDECK_PATH/screenshots"

    mkdir -p "$EMUDECK_PATH/config/Gambatte"
    mkdir -p "$EMUDECK_PATH/config/mGBA"
    mkdir -p "$EMUDECK_PATH/config/Mupen64Plus-Next"
    mkdir -p "$EMUDECK_PATH/config/Snes9x"
    mkdir -p "$EMUDECK_PATH/config/Nestopia"

    rsync -avz --checksum --ignore-existing "$SCRIPT_PATH/assets/roms/" "$EMUDECK_PATH/roms/" > /dev/null 2>&1
fi

if [ -f "$RETROACHIEVEMENTS_FILE" ]; then
    RETROACHIEVEMENTS_USERNAME=$(awk -F "=" '/username/ {print $2}' "$RETROACHIEVEMENTS_FILE" | tr -d ' ')
    RETROACHIEVEMENTS_PASSWORD=$(awk -F "=" '/password/ {print $2}' "$RETROACHIEVEMENTS_FILE" | tr -d ' ')
fi

echo "Installing Emulators"
install_or_update "org.libretro.RetroArch"            "RetroArch"
install_or_update "com.steamgriddb.steam-rom-manager" "Steam ROM Manager"
install_or_update "org.yuzu_emu.yuzu"                 "Yuzu"

echo "Moving RetroArch existing user data"
ra_set_config_file
ra_move_folder "libretro_directory"     "$EMUDECK_PATH/cores"
ra_move_folder "system_directory"       "$EMUDECK_PATH/bios"
ra_move_folder "savefile_directory"     "$EMUDECK_PATH/saves/retroarch/saves"
ra_move_folder "savestate_directory"    "$EMUDECK_PATH/saves/retroarch/states"
ra_move_folder "rgui_config_directory"  "$EMUDECK_PATH/config"
ra_move_folder "screenshot_directory"   "$EMUDECK_PATH/screenshots"

echo "Configuring RetroArch"
# Video
ra_set_config "video_fullscreen"                "true"
ra_set_config "video_scale"                     "1"
ra_set_config "video_adaptive_vsync"            "true"
ra_set_config "video_driver"                    "vulkan"
# Input
ra_set_config "input_joypad_driver"             "sdl2"
ra_set_config "input_menu_toggle_gamepad_combo" "6" # L1+R1
ra_set_config "input_max_users"                 "4"
ra_set_config "menu_swap_ok_cancel_buttons"     "true"
# Directories
ra_set_config "rgui_browser_directory"  "$EMUDECK_PATH/roms"
ra_set_config "libretro_directory"      "$EMUDECK_PATH/cores"
ra_set_config "system_directory"        "$EMUDECK_PATH/bios"
ra_set_config "savefile_directory"      "$EMUDECK_PATH/saves/retroarch/saves"
ra_set_config "savestate_directory"     "$EMUDECK_PATH/saves/retroarch/states"
ra_set_config "rgui_config_directory"   "$EMUDECK_PATH/config"
ra_set_config "screenshot_directory"    "$EMUDECK_PATH/screenshots"
# RetroAchievements
ra_set_config "cheevos_enable"              "true"
ra_set_config "cheevos_auto_screenshot"     "true"
ra_set_config "cheevos_unlock_sound_enable" "true"
if [[ ! -z "$RETROACHIEVEMENTS_USERNAME" && ! -z "$RETROACHIEVEMENTS_PASSWORD" ]]; then
    ra_set_config "cheevos_username"        "$RETROACHIEVEMENTS_USERNAME"
    ra_set_config "cheevos_password"        "$RETROACHIEVEMENTS_PASSWORD"
fi
# Pixel Perfect
ra_set_config "aspect_ratio_index"            "21"     # 21:"1:1 PAR (4:3 DAR)" | 22:"core provided"
ra_set_config "video_scale_integer"           "true"
ra_set_config "video_scale_integer_overscale" "false"
ra_set_config "video_smooth"                  "false"
# Shaders
ra_set_config "video_shader_enable"             "true"
ra_set_config "video_shader_remember_last_dir"  "true"
ra_set_config "video_shader_watch_files"        "true"
# Notifications
ra_set_config "rgui_show_start_screen"              "false"
ra_set_config "menu_show_load_content_animation"    "false"
ra_set_config "notification_show_autoconfig"        "false"
ra_set_config "notification_show_remap_load"        "false"
ra_set_config "cheevos_verbose_enable"              "false"
# Rewind
ra_set_config "rewind_enable"       "true"
ra_set_config "rewind_granularity"  "2"
# Account
ra_set_config "discord_allow"       "true"
ra_set_config "netplay_nickname"    "igoticecream"
# User interface
ra_set_config "menu_timedate_style"                 "25"    # HH:MM (AM/PM)
ra_set_config "menu_thumbnails"                     "3"     # Boxart
ra_set_config "quick_menu_show_savestate_submenu"   "true"
# Saving
ra_set_config "savestate_auto_index"                "true"
ra_set_config "savestate_auto_load"                 "true"
ra_set_config "savestate_auto_save"                 "true"
ra_set_config "savestate_thumbnail_enable"          "true"
ra_set_config "sort_savefiles_by_content_enable"    "true"
ra_set_config "sort_savestates_by_content_enable"   "true"
ra_set_config "sort_screenshots_by_content_enable"  "true"
ra_set_config "sort_savefiles_enable"               "false"
ra_set_config "sort_savestates_enable"              "false"
# Cheats
ra_set_config "apply_cheats_after_toggle" "true"
# Cores
ra_set_config "check_firmware_before_loading"   "true"
ra_set_config "core_info_cache_enable"          "true"

# BIOS from https://github.com/Abdess/retroarch_system
echo "Installing bios"
ra_set_bios

echo "Configuring RetroArch shaders"
# Vulkan
echo '#reference /app/share/libretro/shaders/shaders_slang/handheld/gameboy.slangp' > "$EMUDECK_PATH/config/Gambatte/gb.slangp"
echo '#reference /app/share/libretro/shaders/shaders_slang/handheld/lcd3x.slangp'   > "$EMUDECK_PATH/config/Gambatte/gbc.slangp"
echo '#reference /app/share/libretro/shaders/shaders_slang/handheld/lcd3x.slangp'   > "$EMUDECK_PATH/config/mGBA/gba.slangp"
echo '#reference /app/share/libretro/shaders/shaders_slang/crt/crt-aperture.slangp' > "$EMUDECK_PATH/config/Mupen64Plus-Next/n64.slangp"
echo '#reference /app/share/libretro/shaders/shaders_slang/crt/crt-aperture.slangp' > "$EMUDECK_PATH/config/Snes9x/snes.slangp"
echo '#reference /app/share/libretro/shaders/shaders_slang/crt/crt-aperture.slangp' > "$EMUDECK_PATH/config/Nestopia/nes.slangp"
# GL (disable vulkan above)
echo '#reference "/app/share/libretro/shaders/shaders_cg/handheld/gb-shader.cgp"'                           > "$EMUDECK_PATH/config/Gambatte/gb.cgp"
echo '#reference "/app/share/libretro/shaders/shaders_cg/handheld/lcd-grid-v2-gbc-color-motionblur.cgp"'    > "$EMUDECK_PATH/config/Gambatte/gbc.cgp"
echo '#reference "/app/share/libretro/shaders/shaders_cg/handheld/lcd-grid-v2-gba-color-motionblur.cgp"'    > "$EMUDECK_PATH/config/mGBA/gba.cgp"

echo "Configuring RetroArch cores"
echo 'rewind_enable = "false"'                  >  "$EMUDECK_PATH/config/Mupen64Plus-Next/n64.cfg"
echo 'video_driver  = "glcore"'                 >> "$EMUDECK_PATH/config/Mupen64Plus-Next/n64.cfg"
echo 'mupen64plus-rdp-plugin = "gliden64"'      >  "$EMUDECK_PATH/config/Mupen64Plus-Next/n64.opt"
echo 'mupen64plus-169screensize = "1280x720"'   >> "$EMUDECK_PATH/config/Mupen64Plus-Next/n64.opt"
echo 'mupen64plus-43screensize = "960x720"'     >> "$EMUDECK_PATH/config/Mupen64Plus-Next/n64.opt"
echo 'mupen64plus-aspect = "4:3"'               >> "$EMUDECK_PATH/config/Mupen64Plus-Next/n64.opt" # "16:9 adjusted"
echo 'mupen64plus-EnableNativeResFactor = "6"'  >> "$EMUDECK_PATH/config/Mupen64Plus-Next/n64.opt"
echo 'mupen64plus-BilinearMode = "3point"'      >> "$EMUDECK_PATH/config/Mupen64Plus-Next/n64.opt"
echo 'mupen64plus-pak1 = "rumble"'              >> "$EMUDECK_PATH/config/Mupen64Plus-Next/n64.opt"
echo 'mupen64plus-pak2 = "rumble"'              >> "$EMUDECK_PATH/config/Mupen64Plus-Next/n64.opt"
echo 'mupen64plus-pak3 = "rumble"'              >> "$EMUDECK_PATH/config/Mupen64Plus-Next/n64.opt"
echo 'mupen64plus-pak4 = "rumble"'              >> "$EMUDECK_PATH/config/Mupen64Plus-Next/n64.opt"
echo 'mgba_color_correction = "GBA"'            >  "$EMUDECK_PATH/config/mGBA/gba.opt"

echo "Configuring Steam ROM Manager"
srm_move_folder "$SCRIPT_PATH/assets/configs/steam-rom-manager"

echo "Configuring CEMU"
cemu_configure

echo "Done"
