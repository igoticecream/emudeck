#!/usr/bin/env bash

set -euo pipefail

SCRIPT_PATH=$(dirname -- "$( readlink -f -- "$0"; )";)

source "$SCRIPT_PATH/functions/helper.sh"
source "$SCRIPT_PATH/functions/helper_ra.sh"

trap err_report ERR

echo "Setting up emulation folder on microsd"
MICROSD_PATH=$(get_microsd_path)
EMUDECK_PATH="$MICROSD_PATH/Emulation"

if [ ! -d "$MICROSD_PATH" ]; then
    echo "MicroSD not found"
    exit 1
else
    mkdir -p "$EMUDECK_PATH/cores"
fi

echo "Installing cores"
ra_cores_update

echo "Done"
