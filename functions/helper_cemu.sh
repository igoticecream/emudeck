#!/usr/bin/env bash

# https://chriztr.github.io/cemu_shader_and_pipeline_caches
# https://github.com/ActualMandM/cemu_graphic_packs

function cemu_configure {
    cemu_set_config_file
    cemu_unpack
    cemu_unpack_cache
    cemu_unpack_controllers
    cemu_unpack_graphics_pack
    cemu_unpack_profiles

    # General
    cemu_set_config "content/gp_download" "true"

    # Paths
    cemu_set_config "content/GamePaths/Entry" "$EMUDECK_PATH/roms/wiiu"
    cemu_set_config "content/mlc_path"        "$EMUDECK_PATH/saves/cemu"

    # Graphics
    cemu_set_config "content/Graphic/api"           "1"
    cemu_set_config "content/Graphic/device"        "00000000040000000000000000000000"
    cemu_set_config "content/Graphic/AsyncCompile"  "true"
    cemu_set_config "content/Graphic/UpscaleFilter" "0"

    # Audio
    cemu_set_config "content/Audio/api"           "3"
    cemu_set_config "content/Audio/TVChannels"    "1"
    cemu_set_config "content/Audio/TVVolume"      "100"
    cemu_set_config "content/Audio/TVDevice"      "alsa_output.pci-0000_04_00.5-platform-acp5x_mach.0.HiFi__hw_acp5x_1__sink"
    cemu_set_config "content/Audio/PadChannels"   "1"
    cemu_set_config "content/Audio/PadVolume"     "100"
    cemu_set_config "content/Audio/PadDevice"     "alsa_output.pci-0000_04_00.5-platform-acp5x_mach.0.HiFi__hw_acp5x_1__sink"
    cemu_set_config "content/Audio/InputChannels" "0"
    cemu_set_config "content/Audio/InputVolume"   "100"
    cemu_set_config "content/Audio/InputDevice"   "alsa_input.pci-0000_04_00.5-platform-acp5x_mach.0.HiFi__hw_acp5x_0__source"

    # GUI
    cemu_set_config "content/GameList/name_width"         "250"
    cemu_set_config "content/GameList/version_width"      "80"
    cemu_set_config "content/GameList/dlc_width"          "80"
    cemu_set_config "content/GameList/game_time_width"    "400"
    cemu_set_config "content/GameList/game_started_width" "200"
    cemu_set_config "content/GameList/region_width"       "100"

    # Mods
    cemu_set_config "content/GraphicPack/Entry" "" "filename" "graphicPacks/downloadedGraphicPacks/BreathOfTheWild/!Override/AmdIntelShadows/rules.txt"
    cemu_set_config "content/GraphicPack/Entry" "" "filename" "graphicPacks/downloadedGraphicPacks/BreathOfTheWild/!Override/IntelShadows/rules.txt"
    cemu_set_config "content/GraphicPack/Entry" "" "filename" "graphicPacks/downloadedGraphicPacks/BreathOfTheWild/Cheats/Durability/rules.txt"
    cemu_set_config "content/GraphicPack/Entry" "" "filename" "graphicPacks/downloadedGraphicPacks/BreathOfTheWild/Workarounds/GrassWorkaround/rules.txt"
    cemu_set_config "content/GraphicPack/Entry" "" "filename" "graphicPacks/downloadedGraphicPacks/BreathOfTheWild/Cheats/InfiniteAmiibo/rules.txt"
    cemu_set_config "content/GraphicPack/Entry" "" "filename" "graphicPacks/downloadedGraphicPacks/BreathOfTheWild/Cheats/InfiniteArrows/rules.txt"
    cemu_set_config "content/GraphicPack/Entry" "" "filename" "graphicPacks/downloadedGraphicPacks/BreathOfTheWild/Enhancements/rules.txt"
    cemu_set_config "content/GraphicPack/Entry" "" "filename" "graphicPacks/downloadedGraphicPacks/BreathOfTheWild/Graphics/rules.txt"
    cemu_set_config "content/GraphicPack/Entry" "" "filename" "graphicPacks/downloadedGraphicPacks/BreathOfTheWild/Mods/ControllerLayout/rules.txt"
    cemu_set_config "content/GraphicPack/Entry" "" "filename" "graphicPacks/downloadedGraphicPacks/BreathOfTheWild/Mods/DrawDistance/rules.txt"
    cemu_set_config "content/GraphicPack/Entry" "" "filename" "graphicPacks/downloadedGraphicPacks/BreathOfTheWild/Mods/ExtendedMemory/rules.txt"
    cemu_set_config "content/GraphicPack/Entry" "" "filename" "graphicPacks/downloadedGraphicPacks/BreathOfTheWild/Mods/FPS++/rules.txt"
    cemu_set_config "content/GraphicPack/Entry" "" "filename" "graphicPacks/downloadedGraphicPacks/BreathOfTheWild/Mods/MenuCursorSpeed/rules.txt"
    cemu_set_config "content/GraphicPack/Entry" "" "filename" "graphicPacks/downloadedGraphicPacks/BreathOfTheWild/Workarounds/AMDShaderCrash/rules.txt"
    cemu_set_config "content/GraphicPack/Entry" "" "filename" "graphicPacks/downloadedGraphicPacks/BreathOfTheWild/Workarounds/CPUOcclusionQuery/rules.txt"
    cemu_set_config "content/GraphicPack/Entry" "" "filename" "graphicPacks/downloadedGraphicPacks/BreathOfTheWild/Workarounds/KakarikoTorchShadows/rules.txt"
    cemu_set_config "content/GraphicPack/Entry" "" "filename" "graphicPacks/downloadedGraphicPacks/BreathOfTheWild/Workarounds/LWZXNullCheck/rules.txt"
    cemu_set_config "content/GraphicPack/Entry" "" "filename" "graphicPacks/downloadedGraphicPacks/BreathOfTheWild/Workarounds/NVIDIAExplosionSmoke/rules.txt"
    cemu_set_config "content/GraphicPack/Entry" "" "filename" "graphicPacks/downloadedGraphicPacks/BreathOfTheWild/Workarounds/NVIDIAStretchedClouds/rules.txt"
    cemu_set_config "content/GraphicPack/Entry" "" "filename" "graphicPacks/downloadedGraphicPacks/BreathOfTheWild/Workarounds/ReshadeCompatibility/rules.txt"

    # Mods config
    cemu_set_mod_config "BreathOfTheWild/Cheats/Durability"     "Durability Modifier"               "Unbreakable"
    cemu_set_mod_config "BreathOfTheWild/Enhancements"          "Depth Of Field"                    "Enabled"
    cemu_set_mod_config "BreathOfTheWild/Enhancements"          "Clarity"                           "Serfrost's Preset (Recommended)"
    cemu_set_mod_config "BreathOfTheWild/Enhancements"          "Reflections"                       "Normal Reflections"
    cemu_set_mod_config "BreathOfTheWild/Enhancements"          "Reflection Range"                  "Medium (8 samples)"
    cemu_set_mod_config "BreathOfTheWild/Enhancements"          "Anisotropic Filtering"             "Medium (2x)"
    cemu_set_mod_config "BreathOfTheWild/Graphics"              "Shadows"                           "Medium (100%, Default)"
    cemu_set_mod_config "BreathOfTheWild/Graphics"              "Ultrawide HUD Mode"                "Edge HUD (Default)"
    cemu_set_mod_config "BreathOfTheWild/Graphics"              "Aspect Ratio"                      "16:9 (Default)"
    cemu_set_mod_config "BreathOfTheWild/Graphics"              "Resolution"                        "1280x720 (HD, Default)"
    cemu_set_mod_config "BreathOfTheWild/Graphics"              "Shadow Draw Distance"              "Medium (Lower Draw Distance, Sharper Shadows)"
    cemu_set_mod_config "BreathOfTheWild/Graphics"              "Anti-Aliasing"                     "Normal FXAA (Default)"
    cemu_set_mod_config "BreathOfTheWild/Mods/DrawDistance"     "Texture Distance Detail (LOD)"     "Normal (Default)"
    cemu_set_mod_config "BreathOfTheWild/Mods/DrawDistance"     "NPC, Enemies And Other Entities"   "Medium (1x)"
    cemu_set_mod_config "BreathOfTheWild/Mods/DrawDistance"     "Terrain, Buildings, Bushes And Other Objects" "Medium (1x)"
    cemu_set_mod_config "BreathOfTheWild/Mods/DrawDistance"     "Trees (2D Billboards)"             "Medium (Default)"
    cemu_set_mod_config "BreathOfTheWild/Mods/DrawDistance"     "Grass Blades Density"              "Medium (Default)"
    cemu_set_mod_config "BreathOfTheWild/Mods/FPS++"            "Fence Type"                        "Performance Fence (Default)"
    cemu_set_mod_config "BreathOfTheWild/Mods/FPS++"            "Mode"                              "Advanced Settings"
    cemu_set_mod_config "BreathOfTheWild/Mods/FPS++"            "FPS Limit"                         "60FPS Limit (Default)"
    cemu_set_mod_config "BreathOfTheWild/Mods/FPS++"            "Framerate Limit"                   "40FPS (ideal for 240/120/60Hz displays)"
    cemu_set_mod_config "BreathOfTheWild/Mods/FPS++"            "Cutscene FPS Limit"                "Automatically Limit In Few Cutscenes (Recommended)"
    cemu_set_mod_config "BreathOfTheWild/Mods/FPS++"            "Debug Options"                     "Disabled (Default)"
    cemu_set_mod_config "BreathOfTheWild/Mods/FPS++"            "Static Mode"                       "Disabled (Default, dynamically adjust game speed)"
    cemu_set_mod_config "BreathOfTheWild/Mods/FPS++"            "Frame Average"                     "8 Frames Averaged (Default)"
    cemu_set_mod_config "BreathOfTheWild/Mods/MenuCursorSpeed"  "Menu Navigation Speed"             "1.0x (Default)"
}

function cemu_set_config_file {
    CEMU_CONFIG_PATH="$EMUDECK_PATH/emulators/cemu/settings.xml"

    if [ ! -f "$CEMU_CONFIG_PATH" ]; then
        mkdir -p $( dirname "$CEMU_CONFIG_PATH" )
        echo '<?xml version="1.0" encoding="UTF-8"?><content/>' > "$CEMU_CONFIG_PATH"
    fi
}

function cemu_set_config {
    if [ -z ${CEMU_CONFIG_PATH+x} ]; then
        return 1
    fi

    IFS="/" read -ra ARRAY <<< "$1"

    local path=""
    local name="${ARRAY[-1]}"
    local value="$2"
    local attr="${3-}"
    local attr_value="${4-}"

    unset ARRAY[-1]

    for i in "${ARRAY[@]}"; do
        if [ ! -z "$path" ]; then
            if [[ "$(xmlstarlet sel --template --value-of "count(/$path/$i)" "$CEMU_CONFIG_PATH")" == "0" ]]; then
                xmlstarlet edit \
                    --inplace \
                    --subnode "/$path" --type elem --name "$i" \
                    "$CEMU_CONFIG_PATH"
            fi
        fi
        path+="/$i"
    done

    if [[ -z "$attr" || -z "$attr_value" ]]; then
        xmlstarlet edit \
            --inplace \
            --delete  "/$path/$name" \
            --subnode "/$path" --type elem --name "$name" --value "$value" \
            "$CEMU_CONFIG_PATH"
    else
        if [[ "$(xmlstarlet sel --template --value-of "count($path[$name/@$attr='$attr_value'])" "$CEMU_CONFIG_PATH")" == "0" ]]; then
            xmlstarlet edit \
                --inplace \
                --delete  "/$path/$name[@$attr='$attr_value']" \
                --subnode "/$path"                    --type 'elem' --name "$name" --value "$value" \
                --insert  "/$path/$name[not(@$attr)]" --type 'attr' --name "$attr" --value "$attr_value" \
                "$CEMU_CONFIG_PATH"
        fi
    fi
}

function cemu_set_mod_config {
    local mod="graphicPacks/downloadedGraphicPacks/$1/rules.txt"
    local key="$2"
    local val="$3"

    xmlstarlet edit \
        --inplace \
        --delete  "/content/GraphicPack/Entry[@filename='$mod']/Preset[category[text()='$key']]" \
        --subnode "/content/GraphicPack/Entry[@filename='$mod']"                         --type 'elem' --name "Preset"   --value "" \
        --subnode "/content/GraphicPack/Entry[@filename='$mod']/Preset[not(./category)]" --type 'elem' --name "category" --value "$key" \
        --subnode "/content/GraphicPack/Entry[@filename='$mod']/Preset[not(./preset)]"   --type 'elem' --name "preset"   --value "$val" \
        "$CEMU_CONFIG_PATH"
}

function cemu_unpack {
    local version="2.0-13"
    local zip="$SCRIPT_PATH/assets/configs/cemu/cemu-$version.zip"
    local bin="$SCRIPT_PATH/assets/configs/cemu/cemu-$version"
    local tmp=$(mktemp -d)

    if unzip "$zip" -d "$tmp" > /dev/null 2>&1; [ $? -ne 0 ]; then
        rm -rf $tmp
        echo "Error: Cannot unzip $zip"
        exit 1
    fi

    if rsync -avz --checksum "$tmp/Cemu_$version/" "$EMUDECK_PATH/emulators/cemu" > /dev/null 2>&1; [ $? -ne 0 ]; then
        rm -rf $tmp
        echo "Error: Cannot copy CEMU into the emulation folder"
        exit 1
    fi

    if rsync -avz --checksum "$bin" "$EMUDECK_PATH/emulators/cemu/Cemu" > /dev/null 2>&1; [ $? -ne 0 ]; then
        rm -rf $tmp
        echo "Error: Cannot copy CEMU binary into the emulation folder"
        exit 1
    fi

    rm -rf $tmp
}

function cemu_unpack_cache {
    local zip="$SCRIPT_PATH/assets/configs/cemu/caches/*.zip"
    local tmp=$(mktemp -d)

    for i in $zip; do
        [ -f "$i" ] || break

        if unzip "$i" -d "$tmp" > /dev/null 2>&1; [ $? -ne 0 ]; then
            rm -rf $tmp
            echo "Error: Cannot unzip $zip"
            exit 1
        fi
    done

    if rsync -avz --checksum --delete-after "$tmp/shaderCache/transferable/" "$EMUDECK_PATH/emulators/cemu/shaderCache/transferable" > /dev/null 2>&1; [ $? -ne 0 ]; then
        rm -rf $tmp
        echo "Error: Cannot copy CEMU shaders into the emulation folder"
        exit 1
    fi

    rm -rf $tmp
}

function cemu_unpack_controllers {
    local source="$SCRIPT_PATH/assets/configs/cemu/controllers/"
    local destination="$EMUDECK_PATH/emulators/cemu/controllerProfiles"

    if rsync -avz --checksum "$source" "$destination" > /dev/null 2>&1; [ $? -ne 0 ]; then
        echo "Error: Cannot sync CEMU folder"
        exit 1
    fi
}

function cemu_unpack_graphics_pack {
    local version="886"
    local zip="$SCRIPT_PATH/assets/configs/cemu/packs/graphicPacks$version.zip"
    local tmp=$(mktemp -d)
    local destination="$EMUDECK_PATH/emulators/cemu/graphicPacks/downloadedGraphicPacks"

    if unzip "$zip" -d "$tmp" > /dev/null 2>&1; [ $? -ne 0 ]; then
        rm -rf $tmp
        echo "Error: Cannot unzip $zip"
        exit 1
    fi

    mkdir -p $destination

    if rsync -avz --checksum --delete-after "$tmp/" "$destination" > /dev/null 2>&1; [ $? -ne 0 ]; then
        rm -rf $tmp
        echo "Error: Cannot copy CEMU into the emulation folder"
        exit 1
    fi

    echo "Cemu Graphic Packs: v$version" > "$destination/version.txt"

    rm -rf $tmp
}

function cemu_unpack_profiles {
    local source="$SCRIPT_PATH/assets/configs/cemu/profiles/"
    local destination="$EMUDECK_PATH/emulators/cemu/gameProfiles"

    if rsync -avz --checksum "$source" "$destination" > /dev/null 2>&1; [ $? -ne 0 ]; then
        echo "Error: Cannot sync CEMU folder"
        exit 1
    fi
}
