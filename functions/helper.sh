#!/usr/bin/env bash

function err_report() {
  echo "Error on line: $(caller)" >&2
}

function mkfile() {
    mkdir -p $( dirname "$1") && touch "$1"
}

function is_empty () {
    [ ! "$(ls -A $1)" ]
}

function is_exists () {
    [ -d "$1" ]
}

function is_symlink () {
    [ -L "$1" ] && [ -d "$1" ]
}

function get_microsd_path() {
    if [ -b "/dev/mmcblk0p1" ]; then
        findmnt -n --raw --evaluate --output=target -S /dev/mmcblk0p1
    fi
}

# File exists: Overwrite
# File not present in source: Don't delete
function sync_overwrite() {
    rsync -avz --checksum source1/ target1/
}

# File exists: Overwrite
# File not present in source: Delete
function sync_overwrite_and_delete() {
    rsync -avz --checksum --delete-after source1/ target1/
}

# File exists: Ignore
# File not present in source: Don't delete
function sync_ignore() {
    rsync -avz --checksum --ignore-existing source1/ target1/
}

# File exists: Ignore
# File not present in source: Delete
function sync_ignore_and_delete() {
    rsync -avz --checksum --ignore-existing --delete-after source1/ target1/
}

function flatpak_is_user () {
    flatpak info $1 --user > /dev/null 2>&1
}

function flatpak_is_system () {
    flatpak info $1 --system > /dev/null 2>&1
}

function flatpak_is_installed () {
    flatpak_is_user $1 || flatpak_is_system $1
}

function flatpak_install() {
    flatpak install flathub $1 --user --assumeyes --or-update > /dev/null 2>&1 && \
    flatpak override $1        --user --filesystem=host       > /dev/null 2>&1 && \
    flatpak override $1        --user --share=network         > /dev/null 2>&1
}

function install_or_update () {
    if ! flatpak_is_installed $1 ; then
        printf "Installing %-28s" "$2"

        if flatpak_install $1 ; then
            echo "[ OK ]"
        else
            echo "[ FAIL ]"
            exit 1
        fi
    elif flatpak_is_system $1; then
        printf "Updating %-30s" "$2"
        echo "[ SKIPPING ]"
    else
        printf "Updating %-30s" "$2"

        if flatpak_install $1 ; then
            echo "[ OK ]"
        else
            echo "[ FAIL ]"
            exit 1
        fi
    fi
}
