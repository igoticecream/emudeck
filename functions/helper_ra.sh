#!/usr/bin/env bash

function ra_move_folder() {
    local source=$(ra_get_config "$1" | sed -e "s!^~!$HOME!g")
    local destination="$2"

    printf "Moving to %-55s" "$2"

    if [ "$source" != "$destination" ] && is_exists "$source" && ! is_empty "$source"; then
        if rsync -avz --checksum --ignore-existing "$source/" "$destination/" > /dev/null 2>&1; [ $? -eq 0 ]; then
            echo "[ OK ]"
        else
            echo "[ FAIL ]"
            exit 1
        fi
    else
        echo "[ SKIPPING ]"
    fi
}

function ra_get_config() {
    if [ -z ${RETROARCH_CONFIG_PATH+x} ]; then
        return 1
    fi

    local value=$(grep -nw  "$RETROARCH_CONFIG_PATH" -e "$1" | awk -F "=" '/'"$1"'/ {print $2}' | tr -d ' ' | sed 's/^"\|"$//g')

    if [[ -z "$value" && ! -z "${2+x}" ]]; then
        echo "$2"
    else
        echo "$value"
    fi
}

function ra_set_config() {
    if [ -z ${RETROARCH_CONFIG_PATH+x} ]; then
        return 1
    fi

    local line="$1 = \"$2\""
    local lineno=$(grep -nw  "$RETROARCH_CONFIG_PATH" -e "$1" | cut -f1 -d: )

    if [ "$lineno" != "" ]; then
        echo "$lineno" | while read -r i; do
            sed -i "${i} c\\${line}" "$RETROARCH_CONFIG_PATH"
        done
    else
        echo "$line" >> "$RETROARCH_CONFIG_PATH"
    fi
}

function ra_set_config_file() {
    RETROARCH_CONFIG_PATH="$HOME/.var/app/org.libretro.RetroArch/config/retroarch/retroarch.cfg"

    if [ ! -f $RETROARCH_CONFIG_PATH ]; then
        mkfile $RETROARCH_CONFIG_PATH

        ra_set_config "assets_directory"        "/app/share/libretro/assets/"
        ra_set_config "audio_filter_dir"        "/app/lib/retroarch/filters/audio"
        ra_set_config "cheat_database_path"     "/app/share/libretro/database/cht"
        ra_set_config "content_database_path"   "/app/share/libretro/database/rdb"
        ra_set_config "cursor_directory"        "/app/share/libretro/database/cursors"
        ra_set_config "joypad_autoconfig_dir"   "/app/share/libretro/autoconfig"
        ra_set_config "libretro_info_path"      "/app/share/libretro/info"
        ra_set_config "overlay_directory"       "/app/share/libretro/overlays"
        ra_set_config "video_filter_dir"        "/app/lib/retroarch/filters/video"
        ra_set_config "video_shader_dir"        "/app/share/libretro/shaders"
    fi
}

function ra_set_bios() {
    local zip="$SCRIPT_PATH/assets/bios.zip"
    local tmp=$(mktemp -d)

    if unzip "$zip" -d "$tmp" > /dev/null 2>&1; [ $? -ne 0 ]; then
        rm -rf $tmp
        echo "Error: Cannot unzip $zip"
        exit 1
    fi

    if rsync -avz --checksum --delete-after "$tmp/system/" "$EMUDECK_PATH/bios/" > /dev/null 2>&1; [ $? -ne 0 ]; then
        rm -rf $tmp
        echo "Error: Cannot copy the bios into the emulation folder"
        exit 1
    fi

    if rsync -avz --checksum "$tmp/saves/" "$EMUDECK_PATH/saves/retroarch/saves" > /dev/null 2>&1; [ $? -ne 0 ]; then
        rm -rf $tmp
        echo "Error: Cannot copy the bios into the emulation folder"
        exit 1
    fi

    rm -rf $tmp
}

# TODO: Check pinned cores
# TODO: Multi thread?
function ra_cores_update() {
    local tmp=$(mktemp -d)
    local url="http://buildbot.libretro.com/nightly/linux/x86_64/latest"

    if local cores=$(curl --silent --fail --request GET "$url/.index-extended"); [ $? -ne 0 ]; then
        rm -rf $tmp
        echo "Error: Cannot get list of cores from buildbot. Do you have internet?"
        exit 1
    fi

    mkdir "$tmp/cores"

    echo "$cores" | awk -F' ' '{print $3}' | while read core; do
        printf "Downloading core %-45s" "$core"

        if wget "$url/$core" -P $tmp > /dev/null 2>&1; [ $? -ne 0 ]; then
            echo "[ FAIL ]"
            continue
        fi

        if unzip "$tmp/$core" -d "$tmp/cores" > /dev/null 2>&1; [ $? -ne 0 ]; then
            echo "[ FAIL ]"
            continue
        fi

        echo "[ OK ]"
    done

    printf "Moving cores %-49s"

    if rsync -avz --checksum --delete-after "$tmp/cores/" "$EMUDECK_PATH/cores/" > /dev/null 2>&1; [ $? -eq 0 ]; then
        echo "[ OK ]"
    else
        rm -rf $tmp
        echo "[ FAIL ]"
        exit 1
    fi

    rm -rf $tmp
}
