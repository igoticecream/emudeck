#!/usr/bin/env bash

function srm_move_folder() {
    local source="$1"
    local destination="/home/deck/.var/app/com.steamgriddb.steam-rom-manager/config/steam-rom-manager"

    printf "Moving to %-55s" "$destination"

    mkdir -p "$destination"

    if [ "$source" != "$destination" ] && is_exists "$source" && ! is_empty "$source"; then
        if rsync -avz --checksum "$source/" "$destination/" > /dev/null 2>&1; [ $? -eq 0 ]; then
            echo "[ OK ]"
        else
            echo "[ FAIL ]"
            exit 1
        fi
    else
        echo "[ SKIPPING ]"
    fi
}
